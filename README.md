# Node dockerized lambda

## Prerequisites

- Node >= 20
- Docker 4.2.1

### Setup

1. Install node in your machine.
2. Install Docker.
3. Make sure that you have `make` (to use Makefile) installed.

You're ready if you have everything mentioned above

### Instructions to use.

1. Configure your `.env-example`.
2. In your terminal, type `make start`.
3. To test the installation, use `make invoke`. This command will use the event.json file, and you'll be able to see: example": "I am an example event" on your terminal.

### About the solution.

This Dockerized lambda is ready to be used as needed.

Internally uses the following plugins:

- serverless (latest version)
- serverless-offline (latest version)
- makefile (to automate some common commands)

`Note:` I'll add the deployment script in the next versions using GitlabCI.

### Commands and scripts.

- `make clean-all`
    - Used to thoroughly clean your Docker resources. Please exercise caution; only use this if you don't have other Docker images or containers that you want to keep. You can find them in devops/docker-clean.sh. Will delete your local `node_modules` too.
- `make restart:`
    - Use this if you add or modify any Docker configuration. If you edit your .env file, this command won't reload the environment variables into your container.
- `make debug:`
    - Allows you to inspect your container in the specified `WORKDIR` defined in the Dockerfile. You can use it if you encounter issues or difficulties viewing local files within your Docker or for other advanced purposes.