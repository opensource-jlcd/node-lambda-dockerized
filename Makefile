DOCKER_COMPOSE=docker-compose -f ./infrastructure/docker-compose.yml
SERVICE_NAME=landing
FUNCTION_NAME=landing
ENVIRONMENT_FILE=.env
DOCKER_SHELL=/bin/sh

ifneq (“$(wildcard ${ENVIRONMENT_FILE})“,”“)
    include ${ENVIRONMENT_FILE}
endif

set-env:
	@test -e .env || cp .env-example .env

docker-up:
	$(DOCKER_COMPOSE) up

install:
	docker run -it --rm -w /app -v $(PWD):/app node:20.11.0 yarn install

start: set-env install docker-up

restart:
	$(DOCKER_COMPOSE) up --build

logs:
	$(DOCKER_COMPOSE) logs -t -f

clean-all:
	cd devops; \
	sh docker-clean.sh;

debug:
	$(DOCKER_COMPOSE) exec $(SERVICE_NAME) $(DOCKER_SHELL)

invoke:
	$(DOCKER_COMPOSE) exec $(SERVICE_NAME) $(DOCKER_SHELL) -c "serverless invoke local --function $(FUNCTION_NAME) --path ./event.json"

